#Bitbucket Documentation

##Account and Repository Creation

> Navigate to *www.bitbucket.org* and create an account with your GTRI email.

> Choose the option to *Create a repository* and name it accordingly. Contributors can be added later

##Local Repository Creation

> Navigate to the root folder of the project in mind

> In a terminal, run *git init*, which initializes your local repository

> To link your local repository to bitbucket:
>> On your bitbucket repository source code view, click *Clone* and copy the URL in the window

>> In the same terminal where you ran *git init*, run *git remote add origin <your copied url\>*

>> You are now able to push to the bitbucket repository

## Committing to bitbucket

> If you haven't already, change your local current working branch using *git checkout -b <your branch name\>*. Pushing to *master* is possible, but not a good idea for code reviews.
> Run the following to push to the bitbucket repo
>> *git add .* (the "." means all, but you could use a specific file instead of the entire project in your commit)

>> *git commit -m "<commit message\>"*

>> *git push origin <your branch name\>*

## Submitting for code review

> Now that your code is pushed to bitbucket, you can submit it for review by another repo member

> Go to *Pull Requests* in the bitbucket menu for your repository

> Select *Create Pull Request* and choose your branch and the one it should be merged with, (usually master)

> Wait for somebody to approve it and then select *merge*.

> If need be, delete the source branch since it is all in master now anyway