package com.gtri.kirin;

import javax.servlet.annotation.WebServlet;
import java.net.*;
import java.io.*;
import java.util.Collection;
import java.util.Arrays;
import java.util.Locale;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.server.Page;
import eu.maxschuster.vaadin.autocompletetextfield.*;
import eu.maxschuster.vaadin.autocompletetextfield.provider.*;
import com.vaadin.shared.Position;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of an HTML page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    //private String[] values;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        final TestDesign design = new TestDesign();
        Collection<String> cities = Arrays.asList(new String[] {
                "Singapore",
                "Mysore",
                "Mysuru",
                "Bengaluru",
                "Bangalore",
                "Shanghai",
                "Tokyo",
                "New York",
                "Toronto",
                "Atlanta",
                "Seattle",
                "Dublin",
                "Venice"
        });

        AutocompleteSuggestionProvider suggestionProvider = new CollectionSuggestionProvider(cities, MatchMode.CONTAINS, true, Locale.US);

        design.languageField.setSuggestionProvider(suggestionProvider);


        design.submit.addClickListener(new Button.ClickListener() {
            public void buttonClick(ClickEvent event) {
                FormModel m = new FormModel();
                m.firstName = design.firstName.getValue().toString();
                m.lastName = design.lastName.getValue().toString();
                m.checkToggle = design.checkToggle.getValue();
                m.date = design.date.getValue().toString();
                m.dropdown = design.dropdown.getValue().toString();
                m.cityAutoComplete = design.languageField.getValue().toString();
                System.out.println(m);
                //get("https://www.google.com");
            }
        });

        setContent(design);
    }

    protected String get(String url) {
        System.out.println("MAKING REQUEST");
        try {
            URL u = new URL(url);
            URLConnection req = u.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(req.getInputStream()));

            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                System.out.println(inputLine);
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}