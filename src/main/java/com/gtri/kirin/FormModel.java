package com.gtri.kirin;

public class FormModel {
    protected String firstName;
    protected String lastName;
    protected boolean checkToggle;
    protected String date;
    protected String dropdown;
    protected String cityAutoComplete;

    public String toString() {
        return "FormModel" + this.hashCode() + "{\n" +
                "\t \"firstName\": \"" + firstName + "\"\n" +
                "\t \"lastName\": \"" + lastName + "\"\n" +
                "\t \"checkToggle\": \"" + checkToggle + "\"\n" +
                "\t \"date\": \"" + date + "\"\n" +
                "\t \"dropdown\": " + dropdown + "\"\n" +
                "\t \"city\": " + cityAutoComplete + "\"\n" +
                "}";
    }
}